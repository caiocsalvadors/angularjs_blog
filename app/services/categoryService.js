blogApp.service("categoryService", ["$http", function ($http){
    
    var _this = this;

    //Get all categories
    _this.getCategories = function(){
        return $http.get("http://localhost/betsson_angular1/app/assets/json/categories.json")
            .then(function (res) {
                //Filter active categories
                var categories = _this.getActiveCategories(res.data);
                //Sorts the category array
                categories = _this.sortCategories(categories);
                return categories;
            });
    }

    //Gets a specific category
    _this.getCategory = function (category_id) {
        return $http.get("http://localhost/betsson_angular1/app/assets/json/categories.json")
            .then(function (res) {
                var category;
                var categories = res.data;
                //Filter category by id
                var category = categories.filter(function (x) {
                    return x.id == category_id;
                });      
                return category[0];
            });
    }

    //Filter active categories
    _this.getActiveCategories = function (categories) {
        var oldArray = categories;
        var newArray = [];
        var category;

        for (var i = 0; i < oldArray.length; i++) {
            category = oldArray[i];
            if (category.is_active) {
                newArray.push(category);
            }
        }

        return newArray;
    }

    //Sort the array finding parent categories and it childrens
    _this.sortCategories = function (categories) {
        var oldArray = categories;
        var newArray = [];
        var category;

        for (var i = 0; i < oldArray.length; i++) {
            category = oldArray[i];
            if (category.parent_category_id != null) {
                parentIndex = newArray.findIndex(function(x){
                    return x.id == category.parent_category_id;
                });
                if (parentIndex != -1) {
                    newArray.splice(parentIndex + 1, 0, category);
                }
                else {
                    newArray.push(category);
                }
            }
            else {
                newArray.push(category);
            }
        }

        return newArray;
    }

}]);