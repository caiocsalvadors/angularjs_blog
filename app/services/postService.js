blogApp.service('postService', ['$http', '$q', 'categoryService', function ($http, $q, categoryService) {

    var _this = this;
    var categories;

    //Get featured and active posts
    _this.getFeaturedPosts = function(){
        
        return $http.get("http://localhost/betsson_angular1/app/assets/json/posts.json")
            .then(function (res) {
                //Filters active posts
                var posts = _this.getActivePosts(res.data);
                //Filters featured posts
                posts = _this.checkFeatured(posts);
                return posts;
            });
    }

    //Get all active posts by category ID
    _this.getCategoryPosts = function(id){
        //Gets category array
        var promise1 = categoryService.getCategories();
        //Gets posts array
        var promise2 = $http.get("http://localhost/betsson_angular1/app/assets/json/posts.json");
        //Waits until both promises has been resolved and run both on the same time
        return $q.all([promise1, promise2]).then(function (data) {
            _this.categories = data[0];
            //Filters active posts
            var posts = _this.getActivePosts(data[1].data);
            //Filters posts by category
            posts = _this.filterCategoryPosts(posts, id);
            return posts;
        });
    }

    //Get a specific post
    _this.getPost = function (id) {

        return $http.get("http://localhost/betsson_angular1/app/assets/json/posts.json")
            .then(function (res) {
                var posts = res.data;
                //Get the post by ID
                var post = posts.filter(function (x) {
                    return x.id == id;
                });
                return post[0];
            });
    }

    //Filter active posts
    _this.getActivePosts = function (posts) {
        var oldArray = posts;
        var newArray = [];
        var post;

        for (var i = 0; i < oldArray.length; i++) {
            post = oldArray[i];
            if (post.is_active) {
                newArray.push(post);
            }
        }

        return newArray;
    }

    //Filter featured posts
    _this.checkFeatured = function (posts) {
        var oldArray = posts;
        var newArray = [];
        var post;

        for (var i = 0; i < oldArray.length; i++) {
            post = oldArray[i];
            if (post.is_featured) {
                newArray.push(post);
            }
        }

        return newArray;
    }

    //Filter category posts
    _this.filterCategoryPosts = function (posts, category_id) {
        var oldArray = posts;
        var newArray = [];
        var post;

        for (var i = 0; i < oldArray.length; i++) {
            post = oldArray[i];
            if (post.category_id == category_id) {
                newArray.push(post);
            }
            else{
                //Checks if the post belongs to a children category
                if (_this.isChildrenCategoryPost(post, category_id)){
                    newArray.push(post);
                }
            }
        }

        return newArray;
    }

    //Checks if the post belongs to a children category
    _this.isChildrenCategoryPost = function (post, category_id){
        var parentCategories = _this.categories.filter(function(x){
            return x.parent_category_id == category_id;
        });

        for (var i = 0; i < parentCategories.length; i++) {
            parent = parentCategories[i];
            if (post.category_id == parent.id) {
                return true;
            }
        }

        return false;
    }
   

}]);