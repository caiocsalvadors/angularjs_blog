// ROUTES
blogApp.config(function ($routeProvider) {

    $routeProvider

        .when('/', {
            templateUrl: 'app/views/main.html',
            controller: 'mainController'
        })

        .when('/category/:id', {
            templateUrl: 'app/views/category.html',
            controller: 'categoryController'
        })

        .when('/post/:id', {
            templateUrl: 'app/views/single.html',
            controller: 'singleController'
        })

});