var blogApp = angular.module('blogApp', ['ngRoute']);

//Filter to inject HTML
blogApp.filter("trust", ['$sce', function ($sce) {
    return function (htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    }
}]);