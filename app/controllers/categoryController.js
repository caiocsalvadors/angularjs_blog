blogApp.controller('categoryController', 
    ['$scope', '$routeParams', 'categoryService', 'postService', function ($scope, $routeParams, categoryService, postService) {
    
    //Gets active posts from this category
    var promise = postService.getCategoryPosts($routeParams.id);
    promise.then(function (data) {
        $scope.posts = data;
    });

    //Gets the selected category
    var promise2 = categoryService.getCategory($routeParams.id);
    promise2.then(function (data) {
        $scope.category = data;
    });
}]);