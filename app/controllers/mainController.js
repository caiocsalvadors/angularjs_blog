blogApp.controller('mainController', ['$scope', "postService", function ($scope, postService) {

    //Get all active featured posts
    var promise = postService.getFeaturedPosts();
    promise.then(function (data) {
        $scope.posts = data;
    });   

}]);