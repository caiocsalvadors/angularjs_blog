blogApp.controller('singleController', 
    ['$scope', '$routeParams', 'postService', function ($scope, $routeParams, postService) {

    //Get the select post
    var promise = postService.getPost($routeParams.id);
    promise.then(function (data) {
        $scope.post = data;
    });

}]);