blogApp.controller('sidebarController', ['$scope', 'categoryService', function ($scope, categoryService) {

    //Gets all active categories
    var promise = categoryService.getCategories();
    promise.then(function (data) {
        $scope.categories = data;
    });

}]);